+++
title = "About"
date = 2022-08-02T22:02:25+06:00
description = ""
draft = false
toc = false
categories = []
tags = []
images = [
  "https://source.unsplash.com/collection/983219/1600x900"
] # overrides site-wide open graph image
[[copyright]]
  owner = "Fazle Rabbi"
  date = "2022"
  license = "cc-by-nc-sa-4.0"
+++

I am Fazle Rabbi from Bangladesh 🇧🇩

FOSS enthusiast and lifelong ~~learner~~ noob.

contact me at: [fazlerabbi37 at gmail dot com](mailto:fazlerabbi37@gmail.com)

PGP key: [10B4CB96](https://keys.openpgp.org/search?q=E064D822DF9C0DF3A749EA8DF7A3647B10B4CB96)