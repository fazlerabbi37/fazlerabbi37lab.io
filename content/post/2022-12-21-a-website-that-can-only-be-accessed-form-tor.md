+++
title = "A website that can only be accessed form TOR"
date = 2022-12-21T15:07:00+06:00
description = "configuring web server to only allow connection from TOR network"
draft = true
toc = false
categories = []
tags = []
images = [
  "https://source.unsplash.com/collection/983219/1600x900"
] # overrides site-wide open graph image
[[copyright]]
  owner = "Fazle Rabbi"
  date = "2022"
  license = "cc-by-nc-sa-4.0"
+++

![how it started](/static/images/2022-12-21/how-it-started.jpg)

On 31 July, 2022 someone dropped this on a Linux user group that I am in. For those who don't know Bengali, it says:

> **Can anyone make me a website that can not be accessed from anywhere but an onion domain or TOR network? Or how that script to do that would look like?**
> 
> Budget: 50k BDT (Depends) [~ $527]
> 
> Time: 7 days
> 
> Payment method: face to face meetup, contract sign and confirmation (advance payment on discussion)
> 
> Thanks!
> 
> Please discuss everything in comment section instead of DM.

For the sake of completeness I tried to translate the whole message, but the bold part is important here. At first glance, it kinda looked illegal to me because you would want to restrict access to _clearnet_ users if you want to hide something. So me being me, laughed at the OP saying they are probably doing something shady and moved on.

Later that day I realized that it would be a good technical challenge for me to work on. So I got to work and by work I mean start searching with known keywords.

So we have two problems before us:

1. How do we know the client is connecting to our server from TOR network?
2. How do we allow that connection and block others?

The idea I had in mind is that TOR network has exit nodes that are used at the end of the onion router and when someone from TOR connects to a server it uses the IP addresses for those exit nodes. Most web servers has some config to allow and block IP addresses, thus, if we only allow those IPs and block all we are done.

For web server, I choose to work with `apache2`. This choice was time-consuming, but it did help me achieve the goal of _a good technical challenge for me_. Before playing with the cool kids of today's web server aka nginx, HAProxy, Træfik and Caddy, I used to spend a lot of time with Apache. I forgot most of the config and tricks I leaned, as I don't get much time working with Apache. This was a good chance to relearn those.

First things first, we need the list of all TOR exit nodes.
