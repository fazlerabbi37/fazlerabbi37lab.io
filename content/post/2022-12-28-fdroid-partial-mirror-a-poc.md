+++
title = "F-Droid Partial Mirror: A Proof of Concept"
date = 2022-12-28T03:07:00+06:00
description = "an attempt to make a local first mirror for F-Droid to sync selected apps"
draft = false
toc = false
categories = []
tags = ["weekend","f-droid"]
images = [
  "https://source.unsplash.com/collection/983219/1600x900"
] # overrides site-wide open graph image
[[copyright]]
  owner = "Fazle Rabbi"
  date = "2022"
  license = "cc-by-nc-sa-4.0"
+++

People who know me knows that I love Free and Open Source Software (FOSS) ecosystem. I have been using FOSS stuff (sometime at a cost of convenience) for as long as I can remember. [F-Droid](https://f-droid.org) has been an important part of that journey. F-Droid is an open source Android app catalog. From their own website:

_F-Droid is an installable catalog of FOSS (Free and Open Source Software) applications for the Android platform. The client makes it easy to browse, install, and keep track of updates on your device._

From users perspective, on a high level, F-Droid has 2 things:

* Android client(s)
* Server(s) form where the client downloads the app

The Android client first downloads the _index_ file and then updates the latest app list and download updates for installed apps. It might sound familiar for Linux based package manager like `apt` or `dnf` users. The _index_ file download is `apt update` where downloading update for installed apps and installing them is `apt upgrade`.

Just like Linux based package manager, F-Droid has [mirrors too](https://fdroid.gitlab.io/mirror-monitor). Mirrors are used to distribute the content of the original repository which enable faster download (mirrors are generally close to clients) and less resource pressure on the main repo.

Most of the time a mirror is operated by a non-profit organization, volunteers or university where the cost of resources are beard by both donation and for-profit counterparts of the industry. This makes a lot of sense. The non-profit part has the talent, time and man power to maintain the server and mirror but, they are constrained in terms of resources like server, storage and bandwidth. On the other hand, the corporate counterpart can donate money as part of CSR but don't get the time to maintain the mirror. Win-Win for both!

Unfortunately in our country both pieces of the puzzle is lost in their own world. I don't know any non-profit operated-corporate subsidized mirror yet. Let me know if I get it wrong. The only mirrors that are in operation are operated by hosting providers for Alma Linux, CentOS, Debian, Ubuntu or OpenSUS because they need that for their customers. If any distro is less used by their paying customer or more used by non-paying users than it's not synced properly making it outdated thus unusable. It makes sense for the business but hurts the users. But this is a tangent that we can discuss another day.

Getting back to F-Droid mirrors, it is possible to run an F-Droid mirrors very easily following this [guide](https://f-droid.org/en/docs/Running_a_Mirror) from F-Droid docs. Essentially, running any mirror is just doing periodic `rsync` to an upstream repo aka source truth and serving the files via a web server. Then the question arises, if it is too easy why everyone is not doing it? The constraint here is compute resources, specially storage and bandwidth resources. The bandwidth resource depends on the number of users and mirrors (the ratio of it) but the storage resource is ever-increasing. The primary repository for F-Droid (the _repo_ not the archive) requires just over 60GB of disk space in 24K files. This 60GB is trinkets for a country level or even an ISP level mirror but, alas that is not happening. What if we could run a mirror with less space? Just for the apps that are installed in my phone or my families phones aka download the repo partially? That can be accessible inside a LAN, reducing the bandwidth usages and would be a local first mirror?

This is the idea that I came up with last year, around 20-25th Nov, 2021. Wrote a quick script to download all the files related to single app, stood up a web server and tried to download from F-Droid client. To my dismay, there was no increase in download speed and the client was not even hitting the local server. At that point, I stopped working on it but, the idea stayed in my head.

This year on around the same time (Nov 27, 2022), I was cleaning up the old files and the script resurfaced. A few things changed between Nov 25, 2021 vs Nov 27, 2022. I had time (the whole day in fact), better knowledge working with `nginx` and a clear idea to set up the test this time. I made a script to rank the mirrors, cleaned up the `sync` script a bit and added a `docker-compose.yaml`. For the test I collected all the installed apps from all the phones in my home and put them in a list to run the partial mirror and ran the partial mirror. I disconnected my router from internet to solve the issue with always using official mirror and not hitting local one.

With a few tricks and tweaks, it was a speedy success! The mirror was running on my local machine, one hop away from the phone so, the speed of download was just constrained by network interfaces of my local machine, router and phone. The total number of apps, in the list I created, was 92 among them 63 were unique. This alone gives us a 31.53% reduction in bandwidth. This addresses the bandwidth issue but what about space? I downloaded all the APK files for a given app id. Like for F-Droid, I downloaded all APK from version 1.14 to 1.16 with all the `alpha` versions summing up to 12 APKs. The total disk size for 63 apps with all 273 APKs and metadata was 2.8GB!

The complete work is released under GLP 3 license in [fdroid-partial-mirror](https://gitlab.com/fazlerabbi37/fdroid-partial-mirror) repo. Set up your own local repo, open issue or contribute to the code to improve it!

I am really enjoying the flash like improved speed of the app installation! But that's not the end of story though. This is just a proof of concept. There are a couple of things that can be improved:

* The F-Droid official client always tries to connect to the official mirror even if the local mirror is active. I had to deactivate all mirror to make the local one work. This is really annoying. If the main repos are disabled then we can't browse the apps list (the metadata is missing in the local repo). If the main repo is enabled the client never hits the local mirror. A recent release of official client talks about improved mirror use, but I have yet to check.
* The disk size aka space can be decreased still. We are downloading all the APK, but we can constrain it to 3 or 5 as we already have the installed version number.
* The app list generation is still manual, it would be nice to have it automated.
* Integrating the `rank-mirror` into `sync` script.
* Periodic sync with `cron` or `systemd timer`
* Open an issue [here](https://gitlab.com/fazlerabbi37/fdroid-partial-mirror/-/issues) to add more.

I will be posting update as I progress with this mirror! Keep an eye on the blog. 👋️
