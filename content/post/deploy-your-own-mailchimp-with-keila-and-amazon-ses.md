+++
title = "Deploy Your Own Mailchimp With Keila and Amazon SES"
date = 2022-06-24T19:34:08+06:00
description = "deploying Keila with Amazon SES to replace Mailchimp"
draft = true
toc = false
categories = []
tags = []
images = [
  "https://source.unsplash.com/collection/983219/1600x900"
] # overrides site-wide open graph image
[[copyright]]
  owner = "Fazle Rabbi"
  date = "2022"
  license = "cc-by-nc-sa-4.0"
+++
