---
title: "Thirty Days of ML Log Index 0"
date: 2021-07-31T15:27:53+06:00
draft: true
---

Welcome to Thirty Days of ML Log Index 0. Here we will talk about Kaggle's Thirty Days of ML.

Kaggle announced a learning by doing style machine learning course/competition named Thirty Days of ML. I got to know about it a few days ago and signed up for it. The idea is that for the first 2 weeks in part one, a hands-on assignment will show up in our inbox and by solving them we will get to know the essential skills for handling data. This will be followed by a two weeks beginner-friendly competition to test what we have learned so far. Additionally there will be a chance to attend workshops and seminars hosted by data scientists from Google's Developer Experts Program. It starts at August 2nd.

I am super excited to get started and will keep the log updated as I go. I will try to update the log on the same day as the assignments show up but it may get a bit delayed as I need to organize and polish the log before publishing. Until next time have a nice day!
